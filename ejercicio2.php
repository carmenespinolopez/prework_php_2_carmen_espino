<?php


    function cincoVocales ($parametro) {
        //Creamos las variables de cada vocal en FALSE dando por hecho que no están ya que las buscaremos 
        $letraA = FALSE;
        $letraE = FALSE;
        $letraI = FALSE;
        $letraO = FALSE;
        $letraU = FALSE;

        //Entramos en el bucle dando las vueltas = caracteres en el string
        for ($i = 0; $i < strlen($parametro); $i++) {
            //$letra es el caracter que estamos leyendo
            $letra = $parametro[$i];
            //Comprobamos si el caracter que estamos mirando es una de las vocales y si es así pasamos su variable a TRUE
            if ($letra == 'A' || $letra == 'a') {
                $letraA = TRUE;
            }
            if ($letra == 'E' || $letra == 'e') {
                $letraE = TRUE;
            }
            if ($letra == 'I' || $letra == 'i') {
                $letraI = TRUE;
            }
            if ($letra == 'O' || $letra == 'o' ) {
                $letraO = TRUE;
            }
            if ($letra == 'U' || $letra == 'u' ) {
                $letraU = TRUE;
            }
        }

        //Si todas las variables de las vocales son true devolvemos TRUE, si cualquiera de ellas es FALSE se devolverá FALSE
        return $letraA && $letraE && $letraI && $letraO && $letraU;
    }

    /*Junto al form no muestra nada, descarga el PHP, no he encontrado el problema 
    $frase = $_POST('frase');
    //Comprobamos si el resultado devuelto es TRUE (tiene todas las vocales) o FALSE (no las tiene todas)
    $resultado = cincoVocales($frase);*/
   
    $resultado = cincoVocales("AelOU");
    if ($resultado == TRUE) {
        echo "¡LA PALABRA CONTIENE LAS 5 VOCALES! \n";
    } 
    else {
        echo "NO CONTIENE TODAS LAS VOCALES \n";
    }

?>